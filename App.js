import React from "react";
import MapView, { Heatmap, PROVIDER_GOOGLE } from "react-native-maps";
import { StyleSheet, Text, View, Dimensions } from "react-native";
import Posistiondata from "./Location.js";

export default function App() {
  return (
    <View style={styles.container}>
      <MapView
        provider={PROVIDER_GOOGLE}
        region={{
          latitude: 52.379391,
          longitude: 9.743183,
          latitudeDelta: 0.09,
          longitudeDelta: 0.035
        }}
        style={styles.mapStyle}
      >
        <Heatmap
          points={[
            { latitude: 52.3793, longitude: 9.7431, weight: 1 },
            { latitude: 52.3, longitude: 9.7, weight: 3 }
          ]}
          radius={200}
          dissipating={true}
        ></Heatmap>
      </MapView>
      <Posistiondata />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  mapStyle: {
    width: Dimensions.get("window").width,
    height: "75%"
  }
});
